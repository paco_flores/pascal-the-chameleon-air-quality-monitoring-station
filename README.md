# FabAcademy Challenge 2

Air quality sensor co-designed with and for kids.

![PASCAL_GIT](photos/PASCAL_GIT.jpg)

# Concept
Clean air for all: measuring air quality can save lives !
Air pollution is an urgent danger to public health. It is time to take action!

Outdoor air pollution is responsible for 4.2 million deaths and more than 90% of the world’s population lives in areas that exceed World Health Organization guidelines for ambient air quality.

Children born in areas of high air pollution are likely to have several years taken off their life expectancy.

By tackling air quality, we can save lives and we can also inject urgency into the climate change agenda. The air pollution causes are often the same as climate change: transport, power sector and industrial emissions.

Technology allows us to measure our exposure to air pollution much better than ever before and design invite us to find new ways to deploy in mobility, energy and manufacturing.   

What happens when we involve children’s creativity and ancestor’s tales to co-create an educative and communicational artifact?

# Planning & execution
## Chameleon design

What happens when we involve children’s creativity and ancestor’s tales to co-create an educative and communicational artifact?

For the connection between the air pollution measurement and the technology, we decided to use an animalistic analogy to help people and especially kids understand it better. That is why we thought of co-designing the animal with Alexandra a nine year old girl who decided to use a chameleon and named it Pascal. She also created Pascal from plastilline and afterwards we 3D scanned it using the Qlone phone app and Meshmixer for the final details.

Since the Chameleon capacities are changing colors, we decided to use this amazing feature to represent the different air qualities to be measured such as; red: critical, orange: mediocre and green: normal.

![PASCAL_GIT_COLORS](photos/PASCAL_GIT_COLORS.jpg)

We tested the 3d printing model with translucent filament so it could be illuminated and for the final model we used flexible filament and a thick shell so it could be empty inside, and therefore, more flexible

## Leaf design

Following the concept of the Chameleon, we thought it would be a great idea to create a Leaf as Pascal's house and also for the electronics and them to be visible from the bottom so it can be understood how they are connected.

The Leaf was designed and vectorized in Adobe Illustrator and first we did some testing with cardboard to see how the electronics could be better integrated into the leaf size and design.

After that we used a 9 mm plywood and we cutted with the CNC and glued 3 layers, the surface and 2 other layers with an outline so the circuits could be integrated in between, finally we cutted a 6 mm acrylic with the laser machine for the bottom cover. We also used a manual router to raster a lower level to make the LED lights closer to the surface.


![PASCAL_GIT_LEAF](photos/PASCAL_GIT_LEAF.jpg)

## Electronics design
The electronics for this project are relatively straightforward: the input is a particulate matter sensor; the output is Adafruit Neopixel addressable LEDs; and ESP32 microcontroller processes the logic outlined below to map air quality into colors. A 3.7V, 2000 mAh Lithium-Polymer rechargeable battery powers the device.

# System diagram


# Design elements
![base](3d_files/hoja_2.png)

# Fabrication process & materials
## 3D printing
![chameleon](photos/camaleon.jpg)
## CNC
![cnc](photos/cnc.jpg)
## Electronics
![electronics](photos/electronics.jpg)

# Design & fabrication files

# Bill of Materials
Board: [Adafruit HUZZAH32 – ESP32 Feather](https://www.adafruit.com/product/3591)
Sensor: [Sharp dust sensor GP2Y1010AU0F](https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y1010au_appl_e.pdf)
LED strip: [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)
12mm plywood & 3mm acrylic for leaf base
[PLA flex filament, glow in the dark](https://www.smartmaterials3d.com/en/flex-filament)

# Coding logic
![esp32](photos/esp.jpg)

	include Adafruit_NeoPixel.h library
	define pin numbers (21 for LEDs, 5 PWM signal to sensor & A0 analog signal from sensor)
	declare NeoPixel strip object

	function setup{
		initialize NeoPixel strip
		declare the sensor PWM pin as output
	}

	function loop{
		define output_voltage & particle_density variables
		turn on LED pin to sensor
		delay 280 microseconds
		read sensor signal from analog pin
		delay 1 millisecond
		turn OFF LED pin to sensor
		compute output_voltage from analog signal (divide ADC value by 4095 since 12 bit)
		compute particle_density from output_voltage using linear equation from data sheet

		if (dust_density<50) {
			turn LEDs green
		}
		else if (dust_density>100) {
			turn LEDs red
		}
		else {
			turn LEDs orange
		}
		delay 2 seconds
	}

# Photographs of end artifact
![top](photos/top.jpg)
![bottom](photos/bottom.jpg)

# Iteration process
## Iteration 1
Rigid plastic chameleon
Laser-cut plywood & cardboard leaf
Microbit circuit board w/ USB power
Block-based microbit programming

## Iteration 2
Flexible filament, glow in the dark chameleon
CNC machined plywood leaf
ESP32 feather circuit board w/ battery power
Arduino programming with Neopixel library & sensor interface

# Future Development opportunities
Alexandra and her sister Elena had several ideas for additional animals to help them monitor and study the environmental conditions around them.
## Tortoise to measure soil health
Soil health is at once incredibly important and difficult to measure, at it relies on a myriad of factors, from soil microbiome and earthworms to PH and carbon/nitrogen balance. We would like to design a tortoise that could, similarly to Pascal, measure one (or more) aspects of soil health.
## Octopus to measure water pollution
Likewise, water pollution is a multifaceted issue. Some indicators that can be measured are the concentration of dissolved organic matter, conductivity, salinity, total dissolved solids, dissolvedd oxygen levels, and PH. An silicone molded octopus could be designed to measure this and foster water literacy.

## Dog to measure noise levels
Noise pollution affects millions of individuals living in urban environnents. Transport (highways, trainlines and airports) is the number one source of noise pollution. A dog could be designed to measure the noise levels and demand actions from local authorities.

# Links to individual pages
Made with love by [Veronica](https://gitlab.com/ritaveronica.agreda.depazos/mdef-website), [Paco](https://paco_flores.gitlab.io/mdef-2021/) & [Clément](https://clement_rames.gitlab.io/mdef-website/)
